import jwt from "jsonwebtoken";
import ejs from "ejs";
import htmlPdf from "html-pdf";
import AWS from "aws-sdk";
import nodemailer from "nodemailer";
import config from "config";
import { DeliveryMeasurmentConstants } from '../constants';
import uuid from "uuid";
export class CherryUtilities {

  /**
   * get Unique id
   * @returns string
   */
  public static getUUID() {
    return Date.now();
  }


  /**
   * decoded jwt token
   * @param token string
   * @returns object
   */
  public static getDecoded = async (token: any) => {
    return jwt.decode(token);
  };

  /**
   * check Super admin or sub admin
   * @param token string
   * @returns boolean
   */
  public static isAdmin = async (token: any) => {
    const decoded: any = await CherryUtilities.getDecoded(token);

    if (
      decoded.user_type === "Super-Admin" ||
      decoded.user_type === "Sub-Admin"
    )
      return true;
    return false;
  };

  /**
   * 
   * @param pathname HTML path
   * @param params varibales for View/Html
   * @returns 
   */
  public static htmlToPdfBuffer = async (pathname: any, params: any) => {
    const html: any = await ejs.renderFile(pathname, params);
    return new Promise((resolve, reject) => {
      htmlPdf
        .create(html, {
          orientation: "portrait",
        })
        .toBuffer((err, buffer) => {
          if (err) {
            reject(err);
          } else {
            resolve(buffer);
          }
        });
    });
  };

 /**
  * 
  * @param subject string
  * @param html html page
  * @param toAddresses recivers
  * @param attachments attachments
  * @returns 
  */
  public static sendEmailWithAttachments = async (
    subject: any,
    html: any,
    toAddresses: any,
    attachments: any = ''
  ) => {
    // Construct AWS options
    const options: any = {
      secretAccessKey: config.get("AWS.SES.SECRET"),
      accessKeyId: config.get("AWS.SES.ACCESS_KEY"),
      region: config.get("AWS.SES.REGION"),
      endpoint: config.get("AWS.SES.ENDPOINT"),
    };

    // Update the AWS config
    AWS.config.update(options);

    const ses = new AWS.SES();
    const transporter = nodemailer.createTransport({
      SES: ses,
    });
    const mailOptions: any = {
      from: config.get("AWS.SES.SENDER_EMAIL_ADDRESS"),
      subject,
      html,
      to: toAddresses,
      attachments,
    };

    return await new Promise((resolve, reject) => {
      transporter.sendMail(mailOptions, (err, data) => {
        if(err) {
          console.log('Error sending email '+ err);
          reject(err);
        }
        else{
          resolve(data);
        }
      });
    })
  };

 /**
  * 
  * @param message string
  * @param phoneNumber integer
  * @returns 
  */
  public static sendSMS = async (message: any, phoneNumber: any) => {
    const options: any = {
      secretAccessKey: config.get("AWS.SNS.SECRET"),
      accessKeyId: config.get("AWS.SNS.ACCESS_KEY"),
      region: config.get("AWS.SNS.REGION"),
      endpoint: config.get("AWS.SNS.ENDPOINT")
    };
    
    AWS.config.update(options);
    const sns = new AWS.SNS();

    const params: any = {
      Message: message,
      MessageStructure: config.get("AWS.SNS.MESSAGE_TYPE"),
      PhoneNumber: phoneNumber
    };

    return await sns.publish(params).promise();
  }